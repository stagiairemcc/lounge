import React from 'react';
import { Button } from 'reactstrap';
import logo from './logo.svg';
import Acceuil from './component/Acceuil';
import './App.css';

class App extends React.Component {
  onClick(){
    window.location.href="http://localhost:3000/Acceuil";
  }
  render(){
  return (
    <div className="App">
   <Acceuil/>
    </div>
  )};
}

export default App;
/*<div className="App">
<header className="App-header">
  <img src={logo} className="App-logo" alt="logo" />
  <p>
    Edit <code>src/App.js</code> and save to reload.
  </p>
  <a
    className="App-link"
    href="https://reactjs.org"
    target="_blank"
    rel="noopener noreferrer"
  >
    Learn React
  </a>
</header>
</div>*/