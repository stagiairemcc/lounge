import React from 'react';
import {render} from 'react-dom';
//import { createStore } from "redux";
//import {Provider} from 'react-redux'
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import * as serviceWorker from './serviceWorker';

import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';
import Acceuil from './component/Acceuil';
import Page from './component/Page';


import reducer from './store/reducer'
//const store = createStore(reducer);


const Root = () => (
    <Router>
        <Switch>
            <Route exact path="/" component={App}/>
            <Route path="/Acceuil" component={Acceuil}/>
            <Route path="/Page" component={Page}/>
            
        </Switch>
    </Router>
)
render(
    <Router>
        <Root/>
    </Router>,
    document.getElementById('root')
)



//ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
