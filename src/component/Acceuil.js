import React from 'react';
import { Button } from 'reactstrap';
import icons from 'glyphicons';
import logo from './person-with-arms.png';
import logo1 from './supportCC.png';
import logo2 from './teacherC.png';
import logo3 from './chatting-symbol.png';
import logo4 from './hrColor.png';
import logo5 from './lockerC.png';
import logo6 from './monitorC.png';
import logo7 from './shopC.png';
import logo8 from './MCC12.png';
//import Buttons from './Components/Buttons';
import '../Acceuil.css';
//{icons.airplane}
class Buttons extends React.Component {
  onClick(){
    window.location.href="https://supportit-60196.firebaseapp.com/";
  }
  onClick1(){
    window.location.href="https://formations-9bc92.firebaseapp.com/";
  }
    render(){
  return (
      <header className = "Acceuil">
        <div className="container">
          <div className="row">
            <div  className="col flex-container">
              <Button className = "Button-Design"  onClick = {this.onClick} ><img src={logo1} className="target" alt="logo" /><span className="letxt">SUPPORT IT </span></Button>
              <Button className = "Button-Design"  onClick = {this.onClick1} ><img src={logo2} className="target" alt="logo" /><span className="letxt">FORMATION</span></Button>
              <Button className = "Button-Design" ><img src={logo3} className="target" alt="logo" /><span className="letxt"> ON BOARDING</span></Button>
              <Button className = "Button-Design" ><img src={logo4} className="target" alt="logo" /><span className="letxt">RH </span></Button>
            </div>
          </div>
    
          <div class="row">
            <div class="col flex-container">
              <Button className = "Button-Design" ><img src={logo5} className="target" alt="logo" /><span className="letxt">LOCKER</span></Button>
              <Button className = "Button-Design" ><img src={logo} className="target" alt="logo" /><span className="letxt"> BUREAU</span></Button>
              <Button className = "Button-Design" ><img src={logo6} className="target" alt="logo" /><span className="letxt"> VISIO</span></Button>
              <Button className = "Button-Design" ><img src={logo7} className="target" alt="logo" /><span className="letxt">MARKET PLACE </span></Button>
            </div>
          </div>

             <div class="row">
                <div class="col flex-container">
                  <img src={logo8} className="target1" alt="logo" />
                  </div>
             </div>

        </div>
      </header>
  )};
}

export default Buttons;

  
